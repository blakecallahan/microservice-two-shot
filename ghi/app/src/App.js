import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import ShoesList from './ShoesList';
import HatForm from './HatForm';
import ShoeForm from './ShoeForm'


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route index element={<HatsList/>}/>
            <Route path="new" element={<HatForm/>}/>
          </Route>
          <Route path="shoes">
            <Route index element={<ShoesList/>}/>
            <Route path="new" element={<ShoeForm/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
