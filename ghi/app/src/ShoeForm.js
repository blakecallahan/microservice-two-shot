import React, {useEffect, useState} from 'react';

function ShoeForm() {
    const [bins, setBins] = useState([]);

    const [manufacturer, setManufacturer] = useState("");
    const handleManufacturerChange = (event) => {
        setManufacturer(event.target.value);
    }

    const [model_name, setModelName] = useState("");
    const handleModelNameChange = (event) => {
        setModelName(event.target.value);
    }

    const [color, setColor] = useState("");
    const handleColorChange = (event) => {
        setColor(event.target.value);
    }

    const [picture_url, setPictureUrl] = useState("");
    const handlePictureUrlChange = (event) => {
        setPictureUrl(event.target.value);
    }

    const [bin, setBin] = useState("");
    const handleBinChange = (event) => {
        setBin(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.manufacturer = manufacturer;
        data.model_name = model_name;
        data.color = color;
        data.picture_url = picture_url;
        data.bin = bin;

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);

            setManufacturer('');
            setModelName('');
            setColor('');
            setPictureUrl('');
            setBin('');
        }


    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins)

        }

    }
    useEffect(() => {
        fetchData();
      }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe!</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required type="text" name='manufacturer' id="manufacturer" className="form-control"/>
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleModelNameChange} value={model_name} placeholder="Model Name" required type="text" name='model_name' id="model_name" className="form-control"/>
                <label htmlFor="model_name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name='color' id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureUrlChange} value={picture_url} placeholder="Picture Url" required type="text" name='picture_url' id="picture_url" className="form-control"/>
                <label htmlFor="picture_url">Picture Url</label>
              </div>
              <div className="mb-3">
                <select onChange={handleBinChange} value={bin} required id="bin" name='bin' className="form-select">
                  <option value="">Choose a bin</option>
                  {bins.map(bin => {
                    return (
                        <option key={bin.href} value={bin.href}>
                            {bin.id}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}
export default ShoeForm
