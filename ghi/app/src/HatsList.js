import React, {useEffect, useState} from 'react';



function HatsList () {
    const [hats, setHats] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8090/api/hats/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    function deleteHat(id) {
        fetch(`http://localhost:8090/api/hats/${id}`, {
            method: "DELETE"
        }).then((result) => {
            fetchData()
            result.json().then((resp) => {
            console.warn(resp)
        })
    })
}

    return (
        <>
            <table className="table table-hover table-secondary table-striped border border-dark-subtle shadow container-fluid mt-5">
                <thead>
                    <tr>
                    <th>Fabric type</th>
                    <th>Style name</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Location</th>
                    <th>Delete</th>
                    </tr>
                </thead>
                <tbody className="border-top border-dark-subtle">
                    {hats.map(hat => {
                        return (
                            <tr key={hat.id}>
                                <td>{ hat.fabric_type }</td>
                                <td>{ hat.style_name }</td>
                                <td>{ hat.color }</td>
                                <td><img className="img-fluid img-thumbnail" src = { hat.picture_url } ></img></td>
                                <td>{ hat.location.id }</td>
                                <td><button onClick={() => deleteHat(hat.id)}> Delete </button></td>

                            </tr>
                        );
                    })}
                </tbody>
            </table>


        </>
    )
}
export default HatsList
