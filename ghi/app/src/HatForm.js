import React, {useEffect, useState} from 'react';

function HatForm() {
    const [locations, setLocations] = useState([]);

    const [fabricType, setFabricType] = useState("");
    const handleFabricTypeChange =(event) => {
        setFabricType(event.target.value);
    }

    const [styleName, setStyleName] = useState("");
    const handleStyleNameChange = (event) => {
        setStyleName(event.target.value);
    }

    const [color, setColor] = useState("");
    const handleColorChange = (event) => {
        setColor(event.target.value);
    }

    const [pictureUrl, setPictureUrl] = useState("");
    const handlePictureUrlChange = (event) => {
        setPictureUrl(event.target.value);
    }

    const [location, setLocation] = useState("");
    const handleLocationChange = (event) => {
        setLocation(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.fabric_type = fabricType;
        data.style_name = styleName;
        data.color = color;
        data.picture_url = pictureUrl;
        data.location = location;

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-type': 'application/json'
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            setFabricType('');
            setStyleName('');
            setColor('');
            setLocation('');
            setPictureUrl('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat!</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input onChange={handleFabricTypeChange} value={fabricType} placeholder="Fabric Type" required type="text" name='fabricType' id="fabricType" className="form-control"/>
                <label htmlFor="fabricType">Fabric Type</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStyleNameChange} value={styleName} placeholder="Style Name" required type="text" name='styleName' id="styleName" className="form-control"/>
                <label htmlFor="styleName">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name='color' id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureUrlChange} value={pictureUrl} placeholder="Picture Url" required type="text" name='pictureUrl' id="pictureUrl" className="form-control"/>
                <label htmlFor="pictureUrl">Picture Url</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required id="location" name='location' className="form-select">
                  <option value="">Choose a closet location</option>
                  {locations.map(location => {
                    return (
                        <option key={location.href} value={location.href}>
                            {location.id}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}
export default HatForm
