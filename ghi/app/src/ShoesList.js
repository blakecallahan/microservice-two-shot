import React, {useEffect, useState} from 'react';


function ShoesList () {
    const [shoes, setShoes] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
        }
    }

    function deleteShoe(id) {
        fetch(`http://localhost:8080/api/shoes/${id}`, {
            method: 'DELETE'
        }).then((result) => {
            fetchData()
            result.json().then((resp) => {
            console.warn(resp)
        })
    })
    }

    useEffect(() => {
        fetchData();
    }, []);
    return (
        <>
            <table className="table table-hover table-secondary table-striped border border-dark-subtle shadow container-fluid mt-5">
                <thead>
                    <tr>
                    <th>Manufacturer</th>
                    <th>Model Name</th>
                    <th>Color</th>
                    <th>Picture URL</th>
                    <th>Bin</th>
                    <th>Delete</th>
                    </tr>
                </thead>
                <tbody className="border-top border-dark-subtle">
                    {shoes.map(shoe => {
                        return (
                            <tr key={shoe.id}>
                                <td>{ shoe.manufacturer }</td>
                                <td>{ shoe.model_name }</td>
                                <td>{ shoe.color }</td>
                                <td><img className="img-fluid img-thumbnail" src = { shoe.picture_url } ></img></td>
                                <td>{ shoe.bin.id }</td>
                                <td><button onClick={() => deleteShoe(shoe.id)}> Delete </button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    )
}
export default ShoesList
