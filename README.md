# Wardrobify

Team:

* Blake - Hats
* Brad - Shoes

## Design
We plan on using bootstrap for styling.

## Shoes microservice

Created a Shoes model with a BinVO that contained the required properties for the objects.

Used a poller to get updated data from the backend at a 60-second interval.

Wrote view functions to POST and GET, as well as DELETE. Also wrote corresponding React functions to fetch data from the backend for display on the frontend.

Used component states to save UI data in real time.

Used bootstrap for CSS.

## Hats microservice

make a model with fabric, style name, color, pictureUrl, and location in wardrobe.

made a poller to get hats data

used react to display data on frontend

used component states to save data in real time

used request methods to create, delete, and list data

used bootsrap for simplicity
