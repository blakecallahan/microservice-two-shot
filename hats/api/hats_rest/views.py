from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Hat, LocationVO
from common.json import ModelEncoder


# Create your views here.
class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "id",
        "import_href",
        "closet_name",
    ]

class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric_type",
        "style_name",
        "color",
        "picture_url",
        "location",

    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":

        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatEncoder
        )
    else:
        content = json.loads(request.body)


        location_href = content["location"]
        location = LocationVO.objects.get(import_href=location_href)
        content["location"] = location

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def delete_hat(request, pk):
    if request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Error trying to delete hat"})
